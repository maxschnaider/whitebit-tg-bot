# -*- coding: utf-8 -*-

import telebot
import traceback
import threading
from src.models.firebase import Db
from src.models.classes.config import Config
from src.scripts.script_scheduled_messages import ScriptScheduledMessages
from src.scripts.script_bot_polling import ScriptBotPolling


def main():
    config = Config()
    bot = telebot.TeleBot(config.token)
    db = Db()

    def script_scheduled_messages():
        while True:
            try:
                ScriptScheduledMessages(bot, db)
            except Exception:
                print(traceback.format_exc())
                pass

    def script_bot_polling():
        while True:
            try:
                ScriptBotPolling(bot, db)
            except Exception:
                print(traceback.format_exc())
                pass

    script_scheduled_messages_thread = threading.Thread(name='background', target=script_scheduled_messages)
    script_bot_polling_thread = threading.Thread(name='foreground', target=script_bot_polling)
    script_scheduled_messages_thread.start()
    script_bot_polling_thread.start()


if __name__ == '__main__':
    main()

