import traceback
import telebot
from telebot import types
import urllib
from src.models.classes.chat import Chat
from src.models.classes.config import Config
from src.models.firebase import Db
from src.models.classes.scheduled_message import ScheduledMessage


class ScriptBotPolling:
    def __init__(self, bot: telebot.TeleBot, db: Db):
        bot = bot
        db = db
        config = Config()

        @bot.message_handler(commands=['start'])
        def start(message):
            chat = Chat(message.chat)  # добавили в бд
            if len(message.text.split()) > 1:
                param = message.text.split()[1]
                utms = param.split('__')
                for utm in utms:
                    if utm.startswith('tgbot_'):
                        chat.referrer = utm.replace('tgbot_', '')
                        chat.utm_source = 'telegram'
                        chat.utm_medium = 'bot'
                        chat.utm_campaign = 'referral'
                        continue
                    print(utm)
                    key = utm.split('-', maxsplit=1)[0]
                    value = utm.split('-', maxsplit=1)[1]
                    if 'src' in key or 'utm_source' in key:
                        chat.utm_source = value
                    elif 'mdm' in key or 'utm_medium' in key:
                        chat.utm_medium = value
                    elif 'cmpg' in key or 'utm_campaign' in key:
                        chat.utm_campaign = value
                    elif 'id' in key:
                        chat.utm_id = value
                    elif 'creo' in key:
                        chat.utm_creo = value
                    else:
                        continue
                chat.track_utm_stats()
            send_step(chat, step='lang')

        @bot.callback_query_handler(func=lambda call: True)
        def callback_worker(call):
            chat = Chat(call.message.chat)

            # if call.data == 'support':
            #     send_help(chat)
            #     clear_scheduled_message(chat.id, 'motivashka', chat.step)

            if 'lang' in call.data:
                chat.language = call.data.split('_')[1]
                # send_step(chat, step='hi')
            
            send_step(chat, step='finished')

            # elif 'step' in call.data:
            #     step = call.data.split('_')[1]
            #     clear_scheduled_message(chat.id, 'motivashka', step)
            #     if step == 'reg':
            #         send_step(chat, 'kyc')
            #     elif step == 'kyc':
            #         send_step(chat, 'email')
            #     elif step == 'approve':
            #         send_step(chat, 'activation')
            #     elif step == 'activation':
            #         send_step(chat, 'withdraw')

        @bot.message_handler(content_types=["text", "audio", "document", "photo", "sticker", "video", "video_note", "voice", "location", "contact"])
        def get_messages(message):
            chat = Chat(message.chat)

            if chat.id == config.admin_chat_id or chat.id == config.papka_admin_id:
                return admin_command(chat, message)

            # elif chat.step == 'email':
            #     if '@' in message.text:
            #         email = message.text.translate({ord(c): "" for c in "!#$%^&*()[]{};:/<>?\|`~-=_+ "})
            #         if chat.email != email and db.get_chat_id_by_email(email) is not None:
            #             bot.send_message(chat.id, text=config.messages[chat.step][chat.language][1])
            #         else:
            #             if chat.email != email:
            #                 bot.send_message(config.push_chat_id, text=email)
            #             chat.email = email
            #             return send_step(chat, 'approve')
            #     else:
            #         bot.send_message(chat.id, text=config.messages[chat.step][chat.language][1])
            #
            # elif chat.step == 'ref':
            #     if 'referral/' in message.text:
            #         clear_scheduled_message(chat.id, 'motivashka', chat.step)
            #         referral_code = message.text.strip('/').split('/')[-1]
            #         if (chat.referral_code != referral_code and db.get_user_referrals(referral_code) is not None) or\
            #                 chat.referrer == referral_code or\
            #                 len(referral_code) < 2:
            #             bot.send_message(chat.id, text=config.messages[chat.step][chat.language][1])
            #         else:
            #             if chat.referral_code != referral_code:
            #                 bot.send_message(config.push_chat_id,
            #                                  "user " + referral_code + " activate referral program")
            #             chat.referral_code = referral_code
            #             return send_step(chat, step='ref_activate')
            #     else:
            #         bot.send_message(chat.id, text=config.messages[chat.step][chat.language][1])
            #
            # elif config.messages['stats']['en'][0] in message.text or \
            #         config.messages['stats']['ru'][0] in message.text or \
            #         config.messages['stats']['ua'][0] in message.text:
            #     return bot.send_message(chat.id, text=config.messages['stats'][chat.language][1]+str(chat.refs_num))
            #
            # #  входящее сообщение на любом этапе
            # send_help(chat)
            send_step(chat, 'finished')

        def send_step(chat: Chat, step):
            chat.step = step

            if step == 'lang':
                keyboard = types.InlineKeyboardMarkup()
                keyboard.add(
                    types.InlineKeyboardButton(text='English', callback_data='lang_en'),
                    types.InlineKeyboardButton(text='Русский', callback_data='lang_ru'),
                    types.InlineKeyboardButton(text='Українська', callback_data='lang_ua'))
                bot.send_message(chat.id, text=config.messages[step], reply_markup=keyboard)
            else:
                bot.send_message(chat.id, text=config.messages['finished'][chat.language])

            # elif step == 'hi':
            #     bot.send_message(chat.id, text=config.messages[step][chat.language])
            #     send_step(chat, step='reg')
            #
            # elif step == 'finished':
            #     bot.send_message(chat.id, text=config.messages[step][chat.language])
            #
            # elif step == 'reg':
            #     bot.send_message(chat.id, text=config.messages[step][chat.language][0] + config.messages[step]['link'][
            #         0 if chat.referrer == '' else 1].replace('referrer', chat.referrer) + (
            #                                                'utm_source=' + chat.utm_source + '&utm_medium=' + chat.utm_medium + '&utm_campaign=' + chat.utm_campaign if chat.utm_source != '' else ''))
            #     bot.send_message(chat.id, text=config.messages[step][chat.language][1])
            #     bot.forward_message(chat.id, config.assets_chat_id, config.assets_message_id[step][chat.language])
            #     ScheduledMessage(
            #         name='ask_if_done',
            #         chat_id=chat.id,
            #         delay_sec=30,
            #         step=step,
            #         lang=chat.language
            #     ).start()
            #
            # elif step == 'kyc':
            #     bot.send_message(chat.id, text=config.messages[step][chat.language])
            #     bot.forward_message(chat.id, config.assets_chat_id, config.assets_message_id[step][chat.language])
            #     ScheduledMessage(
            #         name='ask_if_done',
            #         chat_id=chat.id,
            #         delay_sec=30,
            #         step=step,
            #         lang=chat.language
            #     ).start()
            #
            # elif step == 'email':
            #     bot.send_message(chat.id, text=config.messages[step][chat.language])
            #
            # elif step == 'approve':
            #     keyboard = types.InlineKeyboardMarkup()
            #     keyboard.add(
            #         types.InlineKeyboardButton(text=config.messages[step]['btns'][chat.language][0],
            #                                    callback_data='step_'+step+'_y'),
            #         types.InlineKeyboardButton(text=config.messages[step]['btns'][chat.language][1],
            #                                    callback_data='support'))
            #     bot.send_message(chat.id, text=config.messages[step][chat.language].replace('example@mail.ru', chat.email), reply_markup=keyboard)
            #
            # elif step == 'activation':
            #     bot.forward_message(chat.id, config.assets_chat_id, config.assets_message_id[step][chat.language][0])
            #     bot.forward_message(chat.id, config.assets_chat_id, config.assets_message_id[step][chat.language][1])
            #     bot.send_message(chat.id, text=config.messages[step][chat.language][0])
            #     ScheduledMessage(
            #         name='ask_if_done',
            #         chat_id=chat.id,
            #         delay_sec=60,
            #         step=step,
            #         lang=chat.language
            #     ).start()
            #
            # elif step == 'withdraw':
            #     bot.send_message(chat.id, text=config.messages[step][chat.language])
            #     bot.forward_message(chat.id, config.assets_chat_id, config.assets_message_id[step][chat.language])
            #     ScheduledMessage(
            #         name='send_step',
            #         text='ref',
            #         chat_id=chat.id,
            #         delay_sec=60,
            #         step=step,
            #         lang=chat.language
            #     ).start()
            #
            # # elif step == 'ref':
            # #     if chat.referrer == '':
            # #         bot.send_message(chat.id, config.messages[step][chat.language][0])
            # #     else:
            # #         bot.send_message(chat.id, config.messages[step][chat.language][1])
            # #         # media
            # #         bot.send_message(chat.id, config.messages[step][chat.language][2])
            # #     bot.forward_message(chat.id, config.assets_chat_id, config.ref_ua_msg_id if chat.language == 'ua' else config.ref_ru_msg_id if chat.language == 'ru' else config.ref_en_msg_id)
            #
            # elif step == 'ref_activate':
            #     keyboard = types.ReplyKeyboardMarkup()
            #     keyboard.add(types.InlineKeyboardButton(text=config.messages['stats'][chat.language][0], callback_data='stats'))
            #     bot.send_message(chat.id, 'https://t.me/whitebit_referral_bot?start=tgbot_'+chat.referral_code+'\n\n'+config.messages[step][chat.language], reply_markup=keyboard)

        def admin_command(chat, message):
            msg = message.caption if message.photo else message.text
            cmd = msg.split()[0]
            if 'пуш' in cmd:
                step = cmd.replace('пуш', '').replace('ру', '').replace('юа', '').replace('ен', '')
                step = int(step) if step != '' else None
                language = 'ru' if 'ру' in cmd else 'ua' if 'юа' in cmd else 'en' if 'ен' in cmd else ''
                text = msg.replace(cmd, '')
                if message.photo is not None:
                    f = open(language+'push.jpg', 'wb')
                    f.write(urllib.request.urlopen(bot.get_file_url(file_id=message.photo[len(message.photo)-1].file_id)).read())
                    f.close()

                chats = db.get_all_chats()
                for chat_id in chats:
                    user_chat = Chat(chat_data=chats[chat_id])
                    if user_chat.id == 0 or \
                            user_chat.id == config.admin_chat_id or \
                            (language is not '' and language != user_chat.language) or \
                            (step is not None and step != user_chat.step_num()):
                        continue
                    ScheduledMessage(
                        name='push_photo' if len(message.photo) > 0 else 'push',
                        delay_sec=1,
                        chat_id=user_chat.id,
                        text=text,
                        lang=language
                    ).start()

            elif 'таблица' in cmd.split()[0]:
                ScheduledMessage(
                    name='table',
                    delay_sec=1,
                    chat_id=chat.id
                ).start()

            elif 'стата' in cmd.split()[0]:
                ScheduledMessage(
                    name='stats',
                    delay_sec=1,
                    chat_id=chat.id,
                    text='ютм' if 'ютм' in cmd else ''
                ).start()

        def send_help(chat):
            bot.send_message(chat.id, text=config.messages['support'][chat.language], parse_mode='HTML')

        # def send_images(chat_id, images_name):
        #     images = []
        #     for image_name in images_name:
        #         images.append(types.InputMediaPhoto(open('./src/assets/images/' + image_name + '.jpg', 'rb'),
        #                                             caption=str(len(images)+1)))
        #     bot.send_chat_action(chat_id, 'upload_photo')
        #     bot.send_media_group(chat_id, images)
        #     # photo = open('./src/assets/images/' + image_name, 'rb')
        #     # bot.send_photo(chat_id, photo)
        #     # photo.close()
        #
        # def send_video(chat_id, video_name):
        #     # f = open('out.mp4', 'wb')
        #     # f.write(urllib.request.urlopen(db.get_media_url(video_name)).read())
        #     # f.close()
        #     bot.send_chat_action(chat_id, 'upload_video')
        #     video = open('./src/assets/videos/' + video_name, 'rb')
        #     bot.send_video(chat_id, video, supports_streaming=True)
        #     video.close()

        def clear_scheduled_message(chat_id, name, step):
            ScheduledMessage(data=db.get_scheduled_message(str(chat_id) + name + str(step))).finish()

        bot.polling(none_stop=True, interval=0)


